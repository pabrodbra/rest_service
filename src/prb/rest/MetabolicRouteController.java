package prb.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/pathways")
	
public class MetabolicRouteController {
	public static Map<String, MetabolicRoute> pathways = new HashMap<String, MetabolicRoute>();
	
	static{
		MetabolicRoute mr1 = new MetabolicRoute();
		mr1.setId("1");
		mr1.setName("Glycolysis / Gluconeogenesis");
		mr1.setDescription("Glycolysis is the process of converting glucose into pyruvate and generating small amounts of ATP (energy) and NADH (reducing power). It is a central pathway that produces important precursor metabolites: six-carbon compounds of glucose-6P and fructose-6P and three-carbon compounds of glycerone-P, glyceraldehyde-3P, glycerate-3P, phosphoenolpyruvate, and pyruvate [MD:M00001]. Acetyl-CoA, another important precursor metabolite, is produced by oxidative decarboxylation of pyruvate [MD:M00307]. When the enzyme genes of this pathway are examined in completely sequenced genomes, the reaction steps of three-carbon compounds from glycerone-P to pyruvate form a conserved core module [MD:M00002], which is found in almost all organisms and which sometimes contains operon structures in bacterial genomes. Gluconeogenesis is a synthesis pathway of glucose from noncarbohydrate precursors. It is essentially a reversal of glycolysis with minor variations of alternative paths [MD:M00003].");
		mr1.setOrganism("Homo Sapiens");
		ArrayList<String> reactions1 = new ArrayList<String>();
		reactions1.add("Glycolysis (Embden-Meyerhof pathway), glucose => pyruvate");
		reactions1.add("Glycolysis, core module involving three-carbon compounds");
		reactions1.add("Gluconeogenesis, oxaloacetate => fructose-6P");
		reactions1.add("Ascorbate biosynthesis, plants, glucose-6P => ascorbate ");
		reactions1.add("PTS system, glucose-specific II component");
		reactions1.add("PTS system, maltose/glucose-specific II component ");
		reactions1.add("PTS system, beta-glucoside (arbutin/salicin/cellobiose)-specific II component");
		reactions1.add("Pyruvate oxidation, pyruvate => acetyl-CoA");
		reactions1.add("PTS system, glucose-specific II component");
		mr1.setReactions(reactions1);
		pathways.put(mr1.getId(), mr1);
		
		MetabolicRoute mr2 = new MetabolicRoute();
		mr2.setId("2");
		mr2.setName("Glucosinolate biosynthesis");
		mr2.setDescription("Glucosinolates are biologically active secondary metabolites found in Brassicaceae (mustard family) and related families.These compounds are genetically variable within plant species and used as natural pesticides, such as against insect herbivores. All glucosinolates share a common structure consisting of a beta-thioglucose moiety, a sulfonated oxime moiety, and a variable aglycone side chain derived from an alpha-amino acid. Genes encoding glucosinolate biosynthetic enzymes have been identified in Arabidopsis thaliana by genetic polymorphisms and loss-of-function mutations. This map shows examples of side chain elongation in methionine-derived glucosinolates and the core pathway for biosynthesis of glucosinolates from amino acids.");
		mr2.setOrganism("Homo Sapiens");
		ArrayList<String> reactions2 = new ArrayList<String>();
		reactions2.add("Glucosinolate biosynthesis, tryptophan => glucobrassicin ");
		mr2.setReactions(reactions2);
		pathways.put(mr2.getId(), mr2);
		

		MetabolicRoute mr3 = new MetabolicRoute();
		mr3.setId("3");
		mr3.setName("Arginine biosynthesis");
		mr3.setDescription("Metabolism; Amino acid metabolism");
		mr3.setOrganism("Mammals");
		ArrayList<String> reactions3 = new ArrayList<String>();
		reactions3.add("Ornithine biosynthesis, glutamate => ornithine");
		reactions3.add("Urea cycle");
		reactions3.add("Ornithine biosynthesis, mediated by LysW, glutamate => ornithine");
		mr3.setReactions(reactions3);
		pathways.put(mr3.getId(), mr3);
	}
	

	@GET
	@Path("/pathways")
	@Produces("text/xml")
	public ArrayList<MetabolicRoute> showPathways(){
		return new ArrayList<MetabolicRoute>(pathways.values());
	}

	@GET
	@Path("/pathways/{id}")
	@Produces("text/xml")
	public MetabolicRoute showSelectedPathways(@PathParam("id") String id){
		return pathways.get(id);
	}
	
	@GET
	@Path("/json/pathways")
	@Produces("application/json")
	public ArrayList<MetabolicRoute> showPathwaysJSON(){
		return new ArrayList<MetabolicRoute>(pathways.values());
	}
	
	@GET
	@Path("/json/pathways/{id}")
	@Produces("application/json")
	public MetabolicRoute showSelectedPathwaysJSON(@PathParam("id") String id){
		return pathways.get(id);
	}
}

