package prb.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/webservice")
	
public class WebController {
	public static Map<String, Employee> employees = new HashMap<String, Employee>();
	
	static{
		Employee employee1 = new Employee();
		employee1.setEmployeeId("111");
		employee1.setEmployeeName("Pablo Rodriguez");
		employee1.setJobType("King");
		employee1.setSalary((long)9001);
		employee1.setAddress("Caracas");
		employees.put(employee1.getEmployeeId(),employee1);

		Employee employee2 = new Employee();
		employee2.setEmployeeId("222");
		employee2.setEmployeeName("Vladimir Kandinsky");
		employee2.setJobType("Dev");
		employee2.setSalary((long)9002);
		employee2.setAddress("Caracas");
		employees.put(employee2.getEmployeeId(),employee2);
	}
	
	@GET
	@Path("/hello")
	@Produces("text/plain")
	public String hello(){
		return "Hello World Blinsk";
	}
	
	@GET
	@Path("/message/{message}")
	@Produces("text/plain")
	public String showMsg(@PathParam("message") String message){
		return message;
	}
	
	@GET
	@Path("/employees")
	@Produces("text/xml")
	public ArrayList<Employee> showEmployees(){
		return new ArrayList<Employee>(employees.values());
	}
	
	@GET
	@Path("/show_employee/{id}")
	@Produces("text/xml")
	public Employee showSelectedEmployee(@PathParam("id") String id){
		return employees.get(id);
	}
	
	@GET
	@Path("/json/employees")
	@Produces("application/json")
	public ArrayList<Employee> showEmployeesJSON(){
		return new ArrayList<Employee>(employees.values());
	}
	
	@GET
	@Path("/json/show_employee/{id}")
	@Produces("application/json")
	public Employee showSelectedEmployeeJSON(@PathParam("id") String id){
		return employees.get(id);
	}
}
